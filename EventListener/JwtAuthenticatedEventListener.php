<?php declare(strict_types=1);
/*******************************************************************************
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\EventListener;


use Exception;
use KaiGrassnick\ExternalApplicationBundle\Package\Doctrine\DynamicDatabaseStorageService;
use KaiGrassnick\ExternalApplicationBundle\Package\Jwt\ExternalJwtUser;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ExternalApplicationEventListener
 *
 * @package KaiGrassnick\ExternalApplicationBundle\EventListener
 */
class JwtAuthenticatedEventListener
{
    /**
     * @var RequestStack
     */
    private RequestStack $requestStack;


    /**
     * JwtAuthenticatedEventListener constructor.
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }


    /**
     * @param JWTAuthenticatedEvent $event
     *
     * @throws Exception
     */
    public function onLexikjwtauthenticationOnjwtauthenticated(JWTAuthenticatedEvent $event)
    {
        $token   = $event->getToken();
        $jwtUser = $token->getUser();

        if (!$jwtUser instanceof ExternalJwtUser) {
            return;
        }

        $request               = $this->requestStack->getCurrentRequest();
        $externalApplicationId = $request->headers->get(DynamicDatabaseStorageService::REQUESTED_EXTERNAL_APPLICATION_ID);

        $userApplicationId = $jwtUser->getApplicationId();
        if ($userApplicationId !== $externalApplicationId) {
            throw new Exception("User requested invalid application");
        }
    }


}
