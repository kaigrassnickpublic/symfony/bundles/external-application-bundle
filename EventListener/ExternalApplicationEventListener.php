<?php declare(strict_types=1);
/*******************************************************************************
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\EventListener;


use KaiGrassnick\ExternalApplicationBundle\Entity\ExternalApplication;
use KaiGrassnick\ExternalApplicationBundle\Package\RequestHelper;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Class ExternalApplicationEventListener
 *
 * @package KaiGrassnick\ExternalApplicationBundle\EventListener
 */
class ExternalApplicationEventListener
{
    /**
     * @var RequestHelper
     */
    private RequestHelper $requestHelper;


    /**
     * ExternalApplicationEventListener constructor.
     *
     * @param RequestHelper $requestHelper
     */
    public function __construct(RequestHelper $requestHelper)
    {
        $this->requestHelper = $requestHelper;
    }


    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        if ($event->getRequestType() != HttpKernelInterface::MASTER_REQUEST) {
            return;
        }

        $requestHelper = $this->requestHelper;

        if (!$requestHelper->isRequestPathInsidePath($event->getRequest()->getPathInfo(), '.*', false)) {
            return;
        }

        $request = $event->getRequest();

        if (!$requestHelper->hasRequiredHeader($request)) {
            $requestHelper->throwExceptionForMissingHeader();
        }

        $credentials         = $requestHelper->getCredentials($request);
        $externalApplication = $requestHelper->getExternalApplication($credentials);
        if (!$externalApplication instanceof ExternalApplication) {
            $requestHelper->throwExceptionForInvalidCredentials();
        }
    }
}
