<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use KaiGrassnick\ExternalApplicationBundle\Entity\DataSourceType;
use Doctrine\Persistence\ObjectManager;

/**
 * Class DataSourceTypeFixtures
 *
 * @package KaiGrassnick\ExternalApplicationBundle\DataFixtures
 */
class DataSourceTypeFixtures extends Fixture
{
    public const IDENTIFIER_MYSQL = "dataSourceTypeMysql";


    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $this->addFixture($manager, self::IDENTIFIER_MYSQL, DataSourceType::IDENTIFIER_MYSQL, "MySQL");
    }


    /**
     * @param ObjectManager $manager
     * @param string        $fixtureIdentifier
     * @param string        $identifier
     * @param string        $name
     */
    private function addFixture(ObjectManager $manager, string $fixtureIdentifier, string $identifier, string $name): void
    {
        $dataSourceType = (new DataSourceType($name, $identifier));

        $manager->persist($dataSourceType);
        $manager->flush();

        $this->addReference($fixtureIdentifier, $dataSourceType);

        return;
    }
}
