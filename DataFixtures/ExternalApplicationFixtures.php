<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use KaiGrassnick\ExternalApplicationBundle\Entity\DataSource;
use KaiGrassnick\ExternalApplicationBundle\Entity\ExternalApplication;

/**
 * Class ExternalApplicationFixtures
 *
 * @package KaiGrassnick\ExternalApplicationBundle\DataFixtures
 */
class ExternalApplicationFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var DataSource $dataSourceRef */
        $dataSourceRef = $this->getReference(DataSourceFixtures::IDENTIFIER_EXTERNAL);

        $name              = "[DEV] Portrait Kai Grassnick";
        $url               = "https://devbox.kai-grassnick.de";
        $customer          = "11004007694729217";
        $clientId          = "66d11b78-ff7a-481b-9c0b-0ef5f9e2ca7c";
        $clientSecret      = "21c23f07-8acf-4a12-b766-936c163d81b7";
        $masterApplication = true;

        $externalApplication = (new ExternalApplication($name, $url, $customer, $clientId, $clientSecret, $masterApplication))
            ->setDataSource($dataSourceRef);

        $manager->persist($externalApplication);
        $manager->flush();
    }


    /**
     * @return array|string[]
     */
    public function getDependencies(): array
    {
        return [
            DataSourceFixtures::class,
        ];
    }
}
