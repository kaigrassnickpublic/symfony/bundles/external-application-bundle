<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use KaiGrassnick\ExternalApplicationBundle\Entity\DataSource;
use KaiGrassnick\ExternalApplicationBundle\Entity\DataSourceType;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class DataSourceFixtures
 *
 * @package KaiGrassnick\ExternalApplicationBundle\DataFixtures
 */
class DataSourceFixtures extends Fixture implements DependentFixtureInterface
{
    public const IDENTIFIER_KAI      = "dataSourceKai";
    public const IDENTIFIER_EXTERNAL = "dataSourceExternal";


    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var DataSourceType $dataSourceTypeRef */
        $dataSourceTypeRef = $this->getReference(DataSourceTypeFixtures::IDENTIFIER_MYSQL);

        $name         = "[DEV] Database Portrait Kai Grassnick";
        $host         = "tigger.kai-grassnick.de";
        $port         = 3306;
        $username     = "KaiGrassnicksql1";
        $password     = "qvJuet";
        $databaseName = "KaiGrassnicksql1";

        $dataSource = (new DataSource($name, $host, $port, $username, $password, $databaseName, $dataSourceTypeRef));

        $manager->persist($dataSource);
        $manager->flush();

        $this->addReference(self::IDENTIFIER_KAI, $dataSource);

        $name         = "[EXTERNAL] Database test";
        $host         = "mysql-external";
        $port         = 3306;
        $username     = "external";
        $password     = "external";
        $databaseName = "external";

        $dataSource = (new DataSource($name, $host, $port, $username, $password, $databaseName, $dataSourceTypeRef));

        $manager->persist($dataSource);
        $manager->flush();

        $this->addReference(self::IDENTIFIER_EXTERNAL, $dataSource);
    }


    /**
     * @return array|string[]
     */
    public function getDependencies(): array
    {
        return [
            DataSourceTypeFixtures::class,
        ];
    }
}
