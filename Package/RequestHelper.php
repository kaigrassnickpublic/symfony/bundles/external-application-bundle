<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Package;


use Doctrine\ORM\EntityManagerInterface;
use KaiGrassnick\ExternalApplicationBundle\Entity\ExternalApplication;
use KaiGrassnick\ExternalApplicationBundle\Package\Doctrine\DynamicDatabaseStorageService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * Class RequestHelper
 *
 * @package KaiGrassnick\ExternalApplicationBundle\Package
 */
class RequestHelper
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;


    /**
     * RequestHelper constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @param string $requestPath
     * @param string $path
     * @param bool   $regexQuotePath
     *
     * @return bool
     */
    public function isRequestPathInsidePath(string $requestPath, string $path, bool $regexQuotePath = true): bool
    {
        $path    = $regexQuotePath ? preg_quote($path, '/') : $path;
        $pattern = sprintf("/^\/external\/api\/v\d+\/%s$/i", $path);

        return (bool)preg_match($pattern, $requestPath);
    }


    /**
     * @param Request $request
     *
     * @return bool
     */
    public function hasRequiredHeader(Request $request): bool
    {
        return $request->headers->has(DynamicDatabaseStorageService::REQUESTED_EXTERNAL_APPLICATION_ID);
    }


    /**
     * @param Request $request
     *
     * @return array
     */
    public function getCredentials(Request $request): array
    {
        return [
            "id" => $request->headers->get(DynamicDatabaseStorageService::REQUESTED_EXTERNAL_APPLICATION_ID, null),
        ];
    }


    /**
     * @param array $credentials
     *
     * @return ExternalApplication|null
     */
    public function getExternalApplication(array $credentials): ?ExternalApplication
    {
        if (!isset($credentials["id"]) || $credentials["id"] === null) {
            return null;
        }

        $externalApplication = $this->entityManager->getRepository(ExternalApplication::class)->findOneBy(['clientId' => $credentials["id"]]);
        if (!$externalApplication instanceof ExternalApplication) {
            return null;
        }

        return $externalApplication;
    }


    /**
     * @throws BadCredentialsException
     */
    public function throwExceptionForInvalidCredentials(): JsonResponse
    {
        throw new AccessDeniedHttpException("Invalid credentials.");
    }


    /**
     * @throws BadRequestHttpException
     */
    public function throwExceptionForMissingHeader()
    {
        throw new BadRequestHttpException("Missing Header: " . DynamicDatabaseStorageService::REQUESTED_EXTERNAL_APPLICATION_ID);
    }
}
