<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Package\Jwt;


use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;

/**
 * Class ExternalJwtUser
 *
 * @package KaiGrassnick\ExternalApplicationBundle\Package\Jwt
 */
class ExternalJwtUser implements JWTUserInterface
{
    /**
     * @var string
     */
    private string $username;

    /**
     * @var array
     */
    private array $roles;

    /**
     * @var string
     */
    private string $applicationId;


    /**
     * ExternalJwtUser constructor.
     *
     * @param string $username
     * @param string $applicationId
     * @param array  $roles
     */
    public function __construct(string $username, string $applicationId, array $roles = [])
    {
        $this->username      = $username;
        $this->roles         = $roles;
        $this->applicationId = $applicationId;
    }


    /**
     * @param string $username
     * @param array  $payload
     *
     * @return JWTUserInterface
     */
    public static function createFromPayload($username, array $payload): JWTUserInterface
    {
        $roles = [];
        if (isset($payload['roles']) && is_array($payload['roles'])) {
            $roles = $payload['roles'];
        }

        $applicationId = "";
        if (isset($payload['applicationId'])) {
            $applicationId = $payload['applicationId'];
        }

        return new static($username, $applicationId, $roles);
    }


    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }


    /**
     * @return array|string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }


    /**
     * @return string
     */
    public function getApplicationId(): string
    {
        return $this->applicationId;
    }


    /**
     * @return string|void|null
     */
    public function getPassword()
    {
    }


    /**
     * @return string|void|null
     */
    public function getSalt()
    {
    }


    /**
     * @return void
     */
    public function eraseCredentials()
    {
    }
}
