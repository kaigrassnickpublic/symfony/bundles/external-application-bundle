<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Package\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use KaiGrassnick\ExternalApplicationBundle\Entity\DataSource;
use KaiGrassnick\ExternalApplicationBundle\Entity\ExternalApplication;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class DynamicDatabaseService
 *
 * @package KaiGrassnick\ExternalApplicationBundle\Package\Doctrine
 */
class DynamicDatabaseStorageService
{
    public const REQUESTED_EXTERNAL_APPLICATION_ID = "X-APPLICATION-ID";

    /**
     * @var RequestStack
     */
    private RequestStack $requestStack;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;


    /**
     * DynamicDatabaseService constructor.
     *
     * @param RequestStack           $requestStack
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface        $logger
     */
    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->requestStack  = $requestStack;
        $this->entityManager = $entityManager;
        $this->logger        = $logger;
    }


    /**
     * @param ExternalApplication $externalApplication
     *
     * @return $this
     */
    public function setExternalApplication(ExternalApplication $externalApplication): DynamicDatabaseStorageService
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request instanceof Request) {
            $request = new Request();
            $this->requestStack->push($request);
        }

        $request->headers->set(self::REQUESTED_EXTERNAL_APPLICATION_ID, $externalApplication->getClientId());

        return $this;
    }


    /**
     * @return DataSource|null
     */
    public function getDataSource(): ?DataSource
    {
        $dataSource = $this->getDataSourceFromRequest();
        if ($dataSource !== null) {
            return $dataSource;
        }

        return null;
    }


    /**
     * @return DataSource|null
     */
    private function getDataSourceFromRequest(): ?DataSource
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request instanceof Request) {
            $message = "No current request in requestStack.";
            $this->logger->debug(sprintf("%s[%s]: %s", self::class, __FUNCTION__, $message));

            return null;
        }

        $externalApplicationId = $request->headers->get(self::REQUESTED_EXTERNAL_APPLICATION_ID);
        if ($externalApplicationId === null) {
            $message = self::REQUESTED_EXTERNAL_APPLICATION_ID . " was not given in request.";
            $this->logger->debug(sprintf("%s[%s]: %s", self::class, __FUNCTION__, $message));

            return null;
        }

        return $this->getDataSourceFromApplicationId($externalApplicationId);
    }


    /**
     * @param string $externalApplicationId
     *
     * @return DataSource
     */
    private function getDataSourceFromApplicationId(string $externalApplicationId): DataSource
    {
        /** @var ExternalApplication $externalApplication */
        $externalApplication = $this->entityManager->getRepository(ExternalApplication::class)->findOneBy(['clientId' => $externalApplicationId]);
        if (!$externalApplication instanceof ExternalApplication) {
            $message = "ExternalApplication with id '" . $externalApplicationId . "' was not found.";
            $this->logger->warning(sprintf("%s[%s]: %s", self::class, __FUNCTION__, $message));

            throw new BadRequestHttpException("Requested ExternalApplication is invalid");
        }

        return $externalApplication->getDataSource();
    }
}
