<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Package\Doctrine;

use DateTime;

/**
 * Class CustomHelper
 *
 * @package KaiGrassnick\ExternalApplicationBundle\Package\Doctrine
 */
class CustomHelper
{
    public const NULL_DATE_TIME_STRING = "0000-01-01 00:00:00";

    /**
     * @return DateTime
     */
    static public function nullDateTime(): DateTime
    {
        return new DateTime(self::NULL_DATE_TIME_STRING);
    }
}
