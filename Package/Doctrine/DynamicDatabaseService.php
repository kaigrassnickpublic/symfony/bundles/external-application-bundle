<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Package\Doctrine;

use KaiGrassnick\ExternalApplicationBundle\Entity\DataSource;

/**
 * Class DynamicDatabaseService
 *
 * @package KaiGrassnick\ExternalApplicationBundle\Package\Doctrine
 */
class DynamicDatabaseService
{
    /**
     * @var DynamicDatabaseStorageService
     */
    private DynamicDatabaseStorageService $dynamicDatabaseStorageService;

    /**
     * DynamicDatabaseService constructor.
     *
     * @param DynamicDatabaseStorageService $dynamicDatabaseStorageService
     */
    public function __construct(DynamicDatabaseStorageService $dynamicDatabaseStorageService)
    {
        $this->dynamicDatabaseStorageService = $dynamicDatabaseStorageService;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function getParameter(array $params): array
    {
        $dataSource = $this->dynamicDatabaseStorageService->getDataSource();

        if (!$dataSource instanceof DataSource) {
            return $params;
        }

        $params['driver']   = "pdo_mysql";
        $params['dbname']   = $dataSource->getDatabaseName();
        $params['user']     = $dataSource->getUsername();
        $params['password'] = $dataSource->getPassword();
        $params['host']     = $dataSource->getHost();
        $params['port']     = $dataSource->getPort();

        return $params;

    }
}
