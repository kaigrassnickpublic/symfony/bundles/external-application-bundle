<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Package\Doctrine\Wrapper;

use KaiGrassnick\ExternalApplicationBundle\Package\Doctrine\DynamicDatabaseService;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

/**
 * Class DynamicDatabaseWrapper
 *
 * @package KaiGrassnick\ExternalApplicationBundle\Package\Doctrine\Wrapper
 */
class DynamicDatabaseWrapper extends Connection
{
    /**
     * DynamicDatabaseWrapper constructor.
     *
     * @param Connection             $connection
     * @param DynamicDatabaseService $changeDbService
     *
     * @throws DBALException
     */
    public function __construct(Connection $connection, DynamicDatabaseService $changeDbService)
    {
        $lastParams = $connection->getParams();
        $params     = $changeDbService->getParameter($lastParams);

        if ($params !== $lastParams && $this->isConnected()) {
            $this->close();
        }

        parent::__construct($params, $connection->getDriver(), $connection->getConfiguration(), $connection->getEventManager());
    }
}
