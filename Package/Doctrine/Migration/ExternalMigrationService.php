<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Package\Doctrine\Migration;

use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\Migrations\Configuration\Migration\ExistingConfiguration;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\Migrations\Metadata\Storage\TableMetadataStorageConfiguration;
use Doctrine\Migrations\MigratorConfiguration;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ExternalMigrationService
 *
 * @package KaiGrassnick\ExternalApplicationBundle\Package\Doctrine\Migration
 */
class ExternalMigrationService
{
    public const EXTERNAL_MIGRATIONS_NAMESPACE = "ExternalMigrations";

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var string
     */
    private string $projectDirectory;


    /**
     * ExternalMigrationService constructor.
     *
     * @param EntityManagerInterface $externalEntityManager
     * @param string                 $projectDirectory
     */
    public function __construct(EntityManagerInterface $externalEntityManager, string $projectDirectory)
    {
        $this->entityManager    = $externalEntityManager;
        $this->projectDirectory = $projectDirectory;
    }


    /**
     * @return string
     */
    public function createMigration(): string
    {
        $dependencyFactory  = $this->getDependencyFactory();
        $migrationClassName = $dependencyFactory->getClassNameGenerator()->generateClassName(self::EXTERNAL_MIGRATIONS_NAMESPACE);
        $diffGenerator      = $dependencyFactory->getDiffGenerator();

        $dependencyFactory->getMetadataStorage()->ensureInitialized();

        return $diffGenerator->generate($migrationClassName, null);
    }


    /**
     * @return bool
     */
    public function isUpgradeAvailable(): bool
    {
        $dependencyFactory = $this->getDependencyFactory();
        $statusCalculator  = $dependencyFactory->getMigrationStatusCalculator();
        $newMigrations     = $statusCalculator->getNewMigrations();

        return count($newMigrations) > 0;
    }


    /**
     * @return int
     */
    public function upgrade(): int
    {
        return $this->migrate("latest");
    }


    /**
     * @return int
     */
    public function downgrade(): int
    {
        return $this->migrate("prev");
    }


    /**
     * @return int
     */
    public function reset(): int
    {
        return $this->migrate("first");
    }


    /**
     * @param string $versionAlias
     *
     * @return int
     */
    private function migrate(string $versionAlias): int
    {
        $dependencyFactory = $this->getDependencyFactory();
        $migrator          = $dependencyFactory->getMigrator();

        $planCalculator = $dependencyFactory->getMigrationPlanCalculator();
//        $statusCalculator              = $dependencyFactory->getMigrationStatusCalculator();
//        $executedUnavailableMigrations = $statusCalculator->getExecutedUnavailableMigrations();

        $version = $dependencyFactory->getVersionAliasResolver()->resolveVersionAlias($versionAlias);

        $plan = $planCalculator->getPlanUntilVersion($version);

        $dependencyFactory->getMetadataStorage()->ensureInitialized();

        $migConfiguration = new MigratorConfiguration();
        $migConfiguration->setAllOrNothing(true);

        $migrationStatements = $migrator->migrate($plan, $migConfiguration);

        return count($migrationStatements);
    }


    /**
     * @return DependencyFactory
     */
    private function getDependencyFactory(): DependencyFactory
    {
        $configuration = new Configuration();
        $path          = sprintf("%s/src/ExternalMigrations", $this->projectDirectory);

        $configuration->addMigrationsDirectory(self::EXTERNAL_MIGRATIONS_NAMESPACE, $path);
        $configuration->setMetadataStorageConfiguration(new TableMetadataStorageConfiguration());

//        dump($this->entityManager));

        $entityManagerLoader = new ExistingEntityManager($this->entityManager);
        $configurationLoader = new ExistingConfiguration($configuration);

        return DependencyFactory::fromEntityManager($configurationLoader, $entityManagerLoader);
    }
}
