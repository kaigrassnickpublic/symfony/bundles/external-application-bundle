<?php declare(strict_types=1);

namespace KaiGrassnick\ExternalApplicationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ExternalApplicationBundle
 *
 * @package KaiGrassnick\ExternalApplicationBundle
 */
class ExternalApplicationBundle extends Bundle
{

}
