<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Entity;

use KaiGrassnick\ExternalApplicationBundle\Repository\DataSourceRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Timestampable;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=DataSourceRepository::class)
 */
class DataSource implements Timestampable
{

    use TimestampableEntity;

    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(type="bigint", unique=true, nullable=false)
     * @ORM\CustomIdGenerator(class="KaiGrassnick\DoctrineSnowflakeBundle\Generator\SnowflakeGenerator")
     */
    private string $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $host;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $port;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $password;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $databaseName;

    /**
     * @var DataSourceType
     *
     * @ORM\ManyToOne(targetEntity="KaiGrassnick\ExternalApplicationBundle\Entity\DataSourceType")
     */
    private DataSourceType $dataSourceType;


    /**
     * DataSource constructor.
     *
     * @param string         $name
     * @param string         $host
     * @param int            $port
     * @param string         $username
     * @param string         $password
     * @param string         $databaseName
     * @param DataSourceType $dataSourceType
     */
    public function __construct(string $name, string $host, int $port, string $username, string $password, string $databaseName, DataSourceType $dataSourceType)
    {
        $this->setName($name);
        $this->setHost($host);
        $this->setPort($port);
        $this->setUsername($username);
        $this->setPassword($password);
        $this->setDatabaseName($databaseName);
        $this->setDataSourceType($dataSourceType);
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): DataSource
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }


    /**
     * @param string $host
     *
     * @return $this
     */
    public function setHost(string $host): DataSource
    {
        $this->host = $host;

        return $this;
    }


    /**
     * @return int
     */
    public function getPort(): int
    {
        return $this->port;
    }


    /**
     * @param int $port
     *
     * @return $this
     */
    public function setPort(int $port): DataSource
    {
        $this->port = $port;

        return $this;
    }


    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }


    /**
     * @param string $username
     *
     * @return $this
     */
    public function setUsername(string $username): DataSource
    {
        $this->username = $username;

        return $this;
    }


    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }


    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword(string $password): DataSource
    {
        $this->password = $password;

        return $this;
    }


    /**
     * @return string
     */
    public function getDatabaseName(): string
    {
        return $this->databaseName;
    }


    /**
     * @param string $databaseName
     *
     * @return $this
     */
    public function setDatabaseName(string $databaseName): DataSource
    {
        $this->databaseName = $databaseName;

        return $this;
    }


    /**
     * @return DataSourceType
     */
    public function getDataSourceType(): DataSourceType
    {
        return $this->dataSourceType;
    }


    /**
     * @param DataSourceType $dataSourceType
     *
     * @return $this
     */
    public function setDataSourceType(DataSourceType $dataSourceType): DataSource
    {
        $this->dataSourceType = $dataSourceType;

        return $this;
    }


}
