<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Timestampable;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use KaiGrassnick\ExternalApplicationBundle\Repository\ExternalApplicationRepository;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=ExternalApplicationRepository::class)
 */
class ExternalApplication implements UserInterface, Timestampable
{

    use TimestampableEntity;

    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(type="bigint", unique=true, nullable=false)
     * @ORM\CustomIdGenerator(class="KaiGrassnick\DoctrineSnowflakeBundle\Generator\SnowflakeGenerator")
     */
    private string $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    private string $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    private string $url;

    /**
     * @var DataSource|null
     *
     * @ORM\OneToOne(targetEntity="KaiGrassnick\ExternalApplicationBundle\Entity\DataSource")
     */
    private ?DataSource $dataSource;

    /**
     * @var string
     *
     * @ORM\Column(type="bigint", unique=true, nullable=false)
     */
    private string $customer;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=36, unique=true, nullable=false)
     */
    private string $clientId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=36, unique=true, nullable=false)
     */
    private string $clientSecret;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $masterApplication;


    /**
     * ExternalApplication constructor.
     *
     * @param string      $name
     * @param string      $url
     * @param string      $customer
     * @param string|null $clientId
     * @param string|null $clientSecret
     * @param bool        $masterApplication
     */
    public function __construct(string $name, string $url, string $customer, string $clientId = null, string $clientSecret = null, bool $masterApplication = false)
    {
        $this->setName($name);
        $this->setUrl($url);
        $this->setCustomer($customer);
        $this->regenerateClientId($clientId);
        $this->regenerateClientSecret($clientSecret);
        $this->setMasterApplication($masterApplication);
    }


    /**
     * @param string|null $clientId
     *
     * @return $this
     */
    public function regenerateClientId(?string $clientId = null): ExternalApplication
    {
        if ($clientId === null) {
            $this->clientId = $this->generateUuid();
        } else {
            $this->clientId = $clientId;
        }

        return $this;
    }


    /**
     * @param string|null $clientSecret
     *
     * @return $this
     */
    public function regenerateClientSecret(?string $clientSecret = null): ExternalApplication
    {
        if ($clientSecret === null) {
            $this->clientSecret = $this->generateUuid();
        } else {
            $this->clientSecret = $clientSecret;
        }

        return $this;
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): ExternalApplication
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }


    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl(string $url): ExternalApplication
    {
        $this->url = $url;

        return $this;
    }


    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->getDataSource() instanceof DataSource;
    }


    /**
     * @return DataSource|null
     */
    public function getDataSource(): ?DataSource
    {
        return $this->dataSource;
    }


    /**
     * @param DataSource|null $dataSource
     *
     * @return $this
     */
    public function setDataSource(?DataSource $dataSource): ExternalApplication
    {
        $this->dataSource = $dataSource;

        return $this;
    }


    /**
     * @return string
     */
    public function getCustomer(): string
    {
        return $this->customer;
    }


    /**
     * @param string $customer
     *
     * @return ExternalApplication
     */
    public function setCustomer(string $customer): ExternalApplication
    {
        $this->customer = $customer;

        return $this;
    }


    /**
     * @return bool
     */
    public function isMasterApplication(): bool
    {
        return $this->masterApplication;
    }


    /**
     * @param bool $masterApplication
     *
     * @return ExternalApplication
     */
    public function setMasterApplication(bool $masterApplication): ExternalApplication
    {
        $this->masterApplication = $masterApplication;

        return $this;
    }


    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return [
            "ROLE_EXTERNAL_APPLICATION",
        ];
    }


    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return null
     */
    public function getPassword()
    {
        return $this->getClientSecret();
    }


    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return null
     */
    public function getSalt()
    {
        return null;
    }


    /**
     * Returns the username used to authenticate the user.
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->getClientId();
    }


    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }


    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(): void
    {
    }


    /**
     * @return string
     */
    private function generateUuid(): string
    {
        return uuid_create(UUID_TYPE_RANDOM);
    }

}
