<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Entity;

use KaiGrassnick\ExternalApplicationBundle\Repository\DataSourceTypeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DataSourceTypeRepository::class)
 */
class DataSourceType
{
    public const IDENTIFIER_MYSQL = 'mysql';
//    public const IDENTIFIER_PGSQL = 'pgsql';

    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(type="bigint", unique=true, nullable=false)
     * @ORM\CustomIdGenerator(class="KaiGrassnick\DoctrineSnowflakeBundle\Generator\SnowflakeGenerator")
     */
    private string $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    private string $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    private string $internalIdentifier;


    /**
     * DataSourceType constructor.
     *
     * @param string $name
     * @param string $internalIdentifier
     */
    public function __construct(string $name, string $internalIdentifier)
    {
        $this->setName($name);
        $this->setInternalIdentifier($internalIdentifier);
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): DataSourceType
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return string
     */
    public function getInternalIdentifier(): string
    {
        return $this->internalIdentifier;
    }


    /**
     * @param string $internalIdentifier
     *
     * @return $this
     */
    public function setInternalIdentifier(string $internalIdentifier): DataSourceType
    {
        $this->internalIdentifier = $internalIdentifier;

        return $this;
    }
}
