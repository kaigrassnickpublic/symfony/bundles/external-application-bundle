<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Repository;

use KaiGrassnick\ExternalApplicationBundle\Entity\ExternalApplication;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExternalApplication|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExternalApplication|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExternalApplication[]    findAll()
 * @method ExternalApplication[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExternalApplicationRepository extends ServiceEntityRepository
{
    /**
     * ExternalApplicationRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExternalApplication::class);
    }
}
