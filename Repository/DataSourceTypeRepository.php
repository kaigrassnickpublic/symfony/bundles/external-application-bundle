<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\ExternalApplicationBundle\Repository;

use KaiGrassnick\ExternalApplicationBundle\Entity\DataSourceType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DataSourceType|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataSourceType|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataSourceType[]    findAll()
 * @method DataSourceType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataSourceTypeRepository extends ServiceEntityRepository
{
    /**
     * DataSourceTypeRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataSourceType::class);
    }
}
